package hindex2

import (
	"fmt"
	"testing"
)

var (
	in1a = []int{0, 1, 3, 5, 6}
	out1 = 3
	in2a = []int{1, 1, 3}
	out2 = 1
	in3a = []int{0}
	out3 = 0
	in4a = []int{11, 15}
	out4 = 2
	in5a = []int{1, 2, 2, 2}
	out5 = 2
)

func TestCase1(t *testing.T) {
	res := hIndex(in1a)
	if res != out1 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}
func TestCase2(t *testing.T) {
	res := hIndex(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := hIndex(in3a)
	if res != out3 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func TestCase4(t *testing.T) {
	res := hIndex(in4a)
	if res != out4 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in4a, out4, res))
	}
}

func TestCase5(t *testing.T) {
	res := hIndex(in5a)
	if res != out5 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in5a, out5, res))
	}
}

func BenchmarkIndex(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000275_h-index-II
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkIndex-8   	387001521	         3.155 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	000275_h-index-II	1.843s

	*/
	for n := 0; n < b.N; n++ {
		hIndex(in1a)
	}
}

func BenchmarkIndexFast(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000275_h-index-II
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkIndexFast-8   	808273486	         1.738 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	000275_h-index-II	1.864s
	*/
	for n := 0; n < b.N; n++ {
		hIndexFast(in1a)
	}
}
