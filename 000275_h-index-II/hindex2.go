package hindex2

/*
Given an array of integers citations where citations[i] is the number of citations a researcher received for their ith paper and citations is sorted in ascending order, return the researcher's h-index.

According to the definition of h-index on Wikipedia: The h-index is defined as the maximum value of h such that the given researcher has published at least h papers that have each been cited at least h times.

You must write an algorithm that runs in logarithmic time.

Example 1:

Input: citations = [0,1,3,5,6]
Output: 3
Explanation: [0,1,3,5,6] means the researcher has 5 papers in total and each of them had received 0, 1, 3, 5, 6 citations respectively.
Since the researcher has 3 papers with at least 3 citations each and the remaining two with no more than 3 citations each, their h-index is 3.

Example 2:

Input: citations = [1,2,100]
Output: 2

Constraints:

n == citations.length
1 <= n <= 105
0 <= citations[i] <= 1000
citations is sorted in ascending order.

*/

func hIndex(citations []int) int {
	length := len(citations)
	if citations[length-1] == 0 {
		return 0
	}
	lowerCap := 0
	upperCap := length - 1
	var checkidx int
	for lowerCap < upperCap {
		checkidx = (lowerCap + upperCap) / 2
		// 0, 1, 2, 3, 10, 11, 12
		// 7, 6, 5, 4,  3,  2,  1
		// L        |           R
		//          L           R
		//          L    |      R
		//          L    R

		// 0, 1, 3, 5, 6
		// 5, 4, 3, 2, 1
		// L     |     R
		// L     R
		// L  |  R
		//       LR

		// 1, 2, 2, 2
		// 4, 3, 2, 1
		// L  |     R
		//       L  R
		//       L|  R
		//       LR
		if citations[checkidx] < length-checkidx {
			lowerCap = checkidx + 1
		} else {
			upperCap = checkidx
		}
	}
	return length - lowerCap
}

func hIndexFast(citations []int) int {
	left, right := 0, len(citations)-1

	for left <= right {
		mid := left + (right-left)/2

		if citations[mid] == len(citations)-mid {
			return citations[mid]
		} else if citations[mid] > len(citations)-mid {
			right = mid - 1
		} else {
			left = mid + 1
		}
	}
	return len(citations) - left
}
