package reverse_list

import (
	"fmt"
	"reflect"
	"testing"
)

var (
	in1a = &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val: 4,
					Next: &ListNode{
						Val:  5,
						Next: nil,
					},
				},
			},
		},
	}
	out1 = &ListNode{
		Val: 5,
		Next: &ListNode{
			Val: 4,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val: 2,
					Next: &ListNode{
						Val:  1,
						Next: nil,
					},
				},
			},
		},
	}

	in2a = &ListNode{
		Val: 1,
		Next: &ListNode{
			Val:  2,
			Next: nil,
		},
	}
	out2 = &ListNode{
		Val: 2,
		Next: &ListNode{
			Val:  1,
			Next: nil,
		},
	}

	in3a *ListNode
	out3 *ListNode
)

func TestCase1(t *testing.T) {
	res := reverseList(in1a)
	if !reflect.DeepEqual(out1, res) {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}

func TestCase2(t *testing.T) {
	res := reverseList(in2a)
	if !reflect.DeepEqual(out2, res) {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}

func TestCase3(t *testing.T) {
	res := reverseList(in3a)
	if res != nil {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}

func BenchmarkReverseList(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000206_reverse_linked_list
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkReverseList-8   	569182198	         2.030 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	000206_reverse_linked_list	1.947s
	*/
	for n := 0; n < b.N; n++ {
		reverseList(in2a)
	}
}
func BenchmarkReverseListLeetcodeFastest(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000206_reverse_linked_list
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkReverseListFastest-8   	478605915	         2.367 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	000206_reverse_linked_list	2.024s
	*/
	for n := 0; n < b.N; n++ {
		reverseListFastest(in2a)
	}
}
