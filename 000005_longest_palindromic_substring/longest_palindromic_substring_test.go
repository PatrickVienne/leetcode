package longest_palindromic_substring

import (
	"fmt"
	"testing"
)

var (
	in1a  = "babad"
	out1a = "bab"
	out1b = "bab"

	in2a = "cbbd"
	out2 = "bb"

	in3a = "aacabdkacaa"
	out3 = "aca"

	in4a = "aaaa"
	out4 = "aaaa"
)

func TestCase1(t *testing.T) {
	res := longestPalindrome(in1a)
	if res != out1a || res != out1b {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v or %v, but was: %v", in1a, out1a, out1b, res))
	}
}

func TestCase2(t *testing.T) {
	res := longestPalindrome(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := longestPalindrome(in3a)
	if res != out3 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func TestCase4(t *testing.T) {
	res := longestPalindrome(in4a)
	if res != out4 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in4a, out4, res))
	}
}

func BenchmarkSearchingChallenge(b *testing.B) {
	for n := 0; n < b.N; n++ {
		longestPalindrome(in1a)
	}
}
