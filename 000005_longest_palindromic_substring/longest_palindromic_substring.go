package longest_palindromic_substring

import "fmt"

/*
Given a string s, return the longest
palindromic

substring
 in s.



Example 1:

Input: s = "babad"
Output: "bab"
Explanation: "aba" is also a valid answer.
Example 2:

Input: s = "cbbd"
Output: "bb"


Constraints:

1 <= s.length <= 1000
s consist of only digits and English letters.
*/
func longestPalindrome(s string) string {
	longest := ""
	for middlepoint := range s {
		start := middlepoint
		end := middlepoint
		for {
			if start > 0 && end < len(s)-1 && s[start-1] == s[end+1] {
				start--
				end++
			} else {
				break
			}
		}

		if end+1-start > len(longest) && s[start] == s[end] {
			fmt.Printf("center: %d, longest: %s, start:%d, end:%d\n", middlepoint, s[start:end+1], start, end)
			longest = s[start : end+1]
		}
	}

	for middlepoint := range s {

		start := middlepoint
		end := middlepoint + 1
		if end >= len(s) {
			continue
		}
		if s[start] != s[end] {
			continue
		}

		for {
			if start > 0 && end < len(s)-1 && s[start-1] == s[end+1] {
				start--
				end++
			} else {
				break
			}
		}

		if end+1-start > len(longest) && s[start] == s[end] {
			fmt.Printf("center: %d, longest: %s, start:%d, end:%d\n", middlepoint, s[start:end+1], start, end)
			longest = s[start : end+1]
		}
	}

	return longest
}
