package stringtointegeratoi

import (
	"fmt"
	"testing"
)

var (
	in1a = "42"
	out1 = 42

	in2a = "   -42"
	out2 = -42

	in3a = "4193 with words"
	out3 = 4193

	in4a = "words and 987"
	out4 = 0

	in5a = "-91283472332"
	out5 = -2147483648

	in6a = "91283472332"
	out6 = 2147483647

	in7a = "3.14159"
	out7 = 3

	in8a = "20000000000000000000"
	out8 = 2147483647

	in9a = "+-12"
	out9 = 0
)

func TestCase1(t *testing.T) {
	res := myAtoi(in1a)
	if out1 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := myAtoi(in2a)
	if out2 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := myAtoi(in3a)
	if out3 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func TestCase4(t *testing.T) {
	res := myAtoi(in4a)
	if out4 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in4a, out4, res))
	}
}

func TestCase5(t *testing.T) {
	res := myAtoi(in5a)
	if out5 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in5a, out5, res))
	}

	res = myAtoi(in6a)
	if out6 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in6a, out6, res))
	}
}

func TestCase7(t *testing.T) {
	res := myAtoi(in7a)
	if out7 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in7a, out7, res))
	}
}

func TestCase8(t *testing.T) {
	res := myAtoi(in8a)
	if out8 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in8a, out8, res))
	}
}

func TestCase9(t *testing.T) {
	res := myAtoi(in9a)
	if out9 != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in9a, out9, res))
	}
}

func BenchmarkSearchingChallenge(b *testing.B) {
	for n := 0; n < b.N; n++ {
		myAtoi(in1a)
	}
}
