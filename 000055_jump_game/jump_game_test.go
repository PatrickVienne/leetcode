package jump_game

import (
	"fmt"
	"testing"
)

var (
	in1a  = []int{2, 3, 1, 1, 4}
	out1a = true

	in2a = []int{3, 2, 1, 0, 4}
	out2 = false

	in3a = []int{2, 0}
	out3 = true

	in4a = []int{3, 0, 8, 2, 0, 0, 1}
	out4 = true
)

func TestCase1(t *testing.T) {
	res := canJump(in1a)
	if res != out1a {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1a, res))
	}
}

func TestCase2(t *testing.T) {
	res := canJump(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := canJump(in3a)
	if res != out3 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func TestCase4(t *testing.T) {
	res := canJump(in4a)
	if res != out4 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in4a, out4, res))
	}
}

func BenchmarkSearchingChallenge(b *testing.B) {
	for n := 0; n < b.N; n++ {
		canJump(in1a)
	}
}
