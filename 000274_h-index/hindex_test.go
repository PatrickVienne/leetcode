package hindex

import (
	"fmt"
	"testing"
)

var (
	in1a = []int{3, 0, 6, 1, 5}
	out1 = 3
	in2a = []int{1, 3, 1}
	out2 = 1
	in3a = []int{0}
	out3 = 0
)

func TestCase1(t *testing.T) {
	res := hIndex(in1a)
	if res != out1 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}
func TestCase2(t *testing.T) {
	res := hIndex(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := hIndex(in3a)
	if res != out3 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}
