package main

import (
	"fmt"
	"os"
	"os/signal"
	"time"
)

const channelBufferLength = 1024

type ChannelBuffer struct {
	channel chan byte
	counter int
}

func (r *ChannelBuffer) read() ([]byte, bool) {
	b := make([]byte, 0)
	ok := false
	for data := range r.channel {
		ok = true
		// fmt.Println("indexes:", r.readIndex, r.writeIndex)
		b = append(b, data)
		r.counter += 1
	}
	return b, ok
}

func (r *ChannelBuffer) write(b []byte) {
	for i := range b {
		r.channel <- b[i]
	}
	// fmt.Println("written:", r.buffer)
}

func writeRingBuffer(r *ChannelBuffer) {
	for {
		txt := "help-me!"
		// fmt.Println("write:", txt)
		r.write([]byte(txt))
	}
}

func readRingBuffer(r *ChannelBuffer) {
	for {
		r.read()
		// res, ok := r.read()
		// if ok {
		// 	fmt.Println("read:", res)
		// }
	}
}

func NewRingbuffer() *ChannelBuffer {
	return &ChannelBuffer{channel: make(chan byte, channelBufferLength)}
}

func main() {
	rb := NewRingbuffer()
	go writeRingBuffer(rb)
	go readRingBuffer(rb)

	// Right way to stop the server using a SHUTDOWN HOOK
	// Create a channel to receive OS signals
	c := make(chan os.Signal)

	// Relay os.Interrupt to our channel (os.Interrupt = CTRL+C)
	// Ignore other incoming signals
	signal.Notify(c, os.Interrupt)

	go func() {
		time.Sleep(time.Second * 10)
		c <- os.Interrupt
	}()

	// Block main routine until a signal is received
	// As long as user doesn't press CTRL+C a message
	// is not passed and our main routine keeps running
	<-c
	fmt.Println("count", rb.counter)
}
