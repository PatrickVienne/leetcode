package main

import (
	"fmt"
	"os"
	"os/signal"
	"time"
)

const ringBufferLength = 1024

type RingBuffer struct {
	buffer     [ringBufferLength]byte
	writeIndex int
	readIndex  int
	counter    int
}

func (r *RingBuffer) ringBufferLength() int {
	return ringBufferLength
}

func (r *RingBuffer) read() ([]byte, bool) {
	b := make([]byte, 0)
	ok := false
	for r.readIndex != r.writeIndex {
		ok = true
		// fmt.Println("indexes:", r.readIndex, r.writeIndex)
		b = append(b, r.buffer[r.readIndex])
		r.readIndex += 1
		r.readIndex %= r.ringBufferLength()
		r.counter += 1
	}
	return b, ok
}

func (r *RingBuffer) write(b []byte) {
	for i := range b {
		if ((r.readIndex - 1) % r.ringBufferLength()) == r.writeIndex {
			break
		}
		r.buffer[r.writeIndex] = b[i]
		// fmt.Println("written-now:", r.buffer[r.writeIndex], r.writeIndex, b[i], i)
		r.writeIndex += 1
		r.writeIndex %= r.ringBufferLength()
	}
	// fmt.Println("written:", r.buffer)
}

func writeRingBuffer(r *RingBuffer) {
	for {
		txt := "help-me!"
		// fmt.Println("write:", txt)
		r.write([]byte(txt))
	}
}

func readRingBuffer(r *RingBuffer) {
	for {
		r.read()
		// res, ok := r.read()
		// if ok {
		// 	fmt.Println("read:", res)
		// }
	}
}

func NewRingbuffer() *RingBuffer {
	return &RingBuffer{}
}

func runningSum(nums []int) []int {

	for i := 0; i < len(nums); i++ {
		nums[i] = nums[i] + nums[i-1]
	}
	return nums
}

func main() {
	runningSum([]int{1, 2, 3, 4, 5})

	rb := NewRingbuffer()
	go writeRingBuffer(rb)
	go readRingBuffer(rb)

	// Right way to stop the server using a SHUTDOWN HOOK
	// Create a channel to receive OS signals
	c := make(chan os.Signal)

	// Relay os.Interrupt to our channel (os.Interrupt = CTRL+C)
	// Ignore other incoming signals
	signal.Notify(c, os.Interrupt)

	go func() {
		time.Sleep(time.Second * 10)
		c <- os.Interrupt
	}()

	// Block main routine until a signal is received
	// As long as user doesn't press CTRL+C a message
	// is not passed and our main routine keeps running
	<-c
	fmt.Println("count", rb.counter)
}
