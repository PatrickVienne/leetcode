package sudoku

/*
Write a program to solve a Sudoku puzzle by filling the empty cells.

A sudoku solution must satisfy all of the following rules:

Each of the digits 1-9 must occur exactly once in each row.
Each of the digits 1-9 must occur exactly once in each column.
Each of the digits 1-9 must occur exactly once in each of the 9 3x3 sub-boxes of the grid.
The '.' character indicates empty cells.


Example 1:
Input: board = [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]]
Output: [["5","3","4","6","7","8","9","1","2"],["6","7","2","1","9","5","3","4","8"],["1","9","8","3","4","2","5","6","7"],["8","5","9","7","6","1","4","2","3"],["4","2","6","8","5","3","7","9","1"],["7","1","3","9","2","4","8","5","6"],["9","6","1","5","3","7","2","8","4"],["2","8","7","4","1","9","6","3","5"],["3","4","5","2","8","6","1","7","9"]]
Explanation: The input board is shown above and the only valid solution is shown below:


board.length == 9
board[i].length == 9
board[i][j] is a digit 1-9 or '.'.

*/

func isValidSudokuFastest(board [][]byte) bool {
	R, C, S := [9]uint16{}, [9]uint16{}, [9]uint16{}
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if k, N := (i/3)*3+(j/3), uint16(1)<<(board[i][j]-'0'); board[i][j] != '.' {
				if R[i]&N == N {
					return false
				} else if C[j]&N == N {
					return false
				} else if S[k]&N == N {
					return false
				}
				R[i], C[j], S[k] = R[i]|N, C[j]|N, S[k]|N
			}
		}
	}
	return true
}

func solveSudoku(board [][]byte) {
	// Idea:
	// each position is a 16 bit integer. each bit represents a number.
	// first set all bits
	valueMap := make(map[uint16]byte)
	valueMapReverse := make(map[byte]uint16)
	var i uint8
	for i = 0; i < 9; i++ {
		val := uint16(1) << i
		valueMap[val] = '0' + i + 1
		valueMapReverse['0'+i+1] = val
	}
	openPositions := make(map[int]uint16, 81)
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if board[i][j] == '.' {
				continue
			}
			openPositions[j*9/9+i] = valueMapReverse[board[i][j]]
		}
	}
}
