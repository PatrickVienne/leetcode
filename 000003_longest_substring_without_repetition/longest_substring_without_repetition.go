package longest_substring_without_repetition

import "fmt"

/*
Given a string s, find the length of the longest
substring
 without repeating characters.



Example 1:

Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
Example 2:

Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.


Constraints:

0 <= s.length <= 5 * 104
s consists of English letters, digits, symbols and spaces.
*/
func lengthOfLongestSubstring(s string) int {
	found := make(map[rune]bool)
	ptrBStart := 0
	total := 0
	for ptrA, val := range s {
		found[val] = true
		if len(found) != (ptrA + 1 - ptrBStart) {
			//found duplicate, need to move second pointer forward
			for ptrB, valB := range s[ptrBStart:ptrA] {
				fmt.Printf("invalid: %s, moving forward and delete duplicate, ptrA:%d, ptrB:%d, del:%v\n", s[ptrBStart:ptrA+1], ptrBStart, ptrA, valB)
				delete(found, valB)
				if valB == val {
					ptrBStart = ptrBStart + ptrB + 1
					fmt.Printf("restart with new sub: %s, ptrA:%d, ptrB:%d\n", s[ptrBStart:ptrA+1], ptrBStart, ptrA)
					break
				}
			}
		}
		if ptrA+1-ptrBStart > total {
			total = ptrA + 1 - ptrBStart
			fmt.Printf("current longest: %s, ptrA:%d, ptrB:%d, total:%d\n", s[ptrBStart:ptrA+1], ptrBStart, ptrA, total)

		}
	}
	return total
}
