package longest_substring_without_repetition

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	in1a = "abcabcbb"
	out1 = 3

	in2a = "bbbbb"
	out2 = 1

	in3a = "pwwkew"
	out3 = 3
)

func TestCase1(t *testing.T) {
	res := lengthOfLongestSubstring(in1a)
	if !assert.ElementsMatch(t, out1, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := lengthOfLongestSubstring(in2a)
	if !assert.ElementsMatch(t, out2, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := lengthOfLongestSubstring(in3a)
	if !assert.ElementsMatch(t, out3, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func BenchmarkSearchingChallenge(b *testing.B) {
	for n := 0; n < b.N; n++ {
		lengthOfLongestSubstring(in1a)
	}
}
