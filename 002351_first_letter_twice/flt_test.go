package flt

import (
	"fmt"
	"testing"
)

var (
	in1a      = "abccbaacz"
	out1 byte = 'c'

	in2a      = "abcdd"
	out2 byte = 'd'
)

func TestCase1(t *testing.T) {
	res := repeatedCharacter(in1a)
	if res != out1 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := repeatedCharacter(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func BenchmarkRepeatedCharacter(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: leetcode/002351_first_letter_twice
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkRepeatedCharacter-8   	 3482763	       467.9 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	leetcode/002351_first_letter_twice	2.867s
	*/
	for n := 0; n < b.N; n++ {
		repeatedCharacter(in1a)
	}
}
