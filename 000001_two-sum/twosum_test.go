package twosum

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	in1a = []int{2, 7, 11, 15}
	in1b = 9

	in2a = []int{3, 2, 4}
	in2b = 6

	in3a = []int{3, 3}
	in3b = 6

	out1 = []int{0, 1}
	out2 = []int{1, 2}
	out3 = []int{0, 1}
)

func TestCase1(t *testing.T) {
	res := twoSum(in1a, in1b)
	if !assert.ElementsMatch(t, out1, res) {
		t.Fatalf(fmt.Sprintf("Input: %v %v, Expected: %v, but was: %v", in1a, in1b, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := twoSum(in2a, in2b)
	if !assert.ElementsMatch(t, out2, res) {
		t.Fatalf(fmt.Sprintf("Input: %v %v, Expected: %v, but was: %v", in2a, in2b, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := twoSum(in3a, in3b)
	if !assert.ElementsMatch(t, out3, res) {
		t.Fatalf(fmt.Sprintf("Input: %v %v, Expected: %v, but was: %v", in3a, in3b, out3, res))
	}
}

func BenchmarkSearchingChallenge(b *testing.B) {
	for n := 0; n < b.N; n++ {
		twoSum(in1a, in1b)
	}
}
