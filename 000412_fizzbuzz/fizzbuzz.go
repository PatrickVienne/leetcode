package fizzbuzz

import (
	"fmt"
	"strconv"
)

/*
Given an integer n, return a string array answer (1-indexed) where:

answer[i] == "FizzBuzz" if i is divisible by 3 and 5.
answer[i] == "Fizz" if i is divisible by 3.
answer[i] == "Buzz" if i is divisible by 5.
answer[i] == i (as a string) if none of the above conditions are true.


Example 1:

Input: n = 3
Output: ["1","2","Fizz"]
Example 2:

Input: n = 5
Output: ["1","2","Fizz","4","Buzz"]
Example 3:

Input: n = 15
Output: ["1","2","Fizz","4","Buzz","Fizz","7","8","Fizz","Buzz","11","Fizz","13","14","FizzBuzz"]


Constraints:

1 <= n <= 104

*/

func fizzBuzz(n int) []string {
	res := []string{}
	for i := 1; i <= n; i++ {
		if i%15 == 0 {
			res = append(res, "FizzBuzz")
		} else if i%5 == 0 {
			res = append(res, "Buzz")
		} else if i%3 == 0 {
			res = append(res, "Fizz")
		} else {
			res = append(res, fmt.Sprintf("%d", i))
		}
	}
	return res
}

type F func(str string, val int) string

var conditions = []F{
	func(str string, val int) string {
		if val%3 == 0 {
			return "Fizz"
		}

		return ""
	},
	func(str string, val int) string {
		if val%5 == 0 {
			return "Buzz"
		}

		return ""
	},
	func(str string, val int) string {
		if len(str) == 0 {
			return strconv.Itoa(val)
		}

		return ""
	},
}

func fastBuzz(n int) []string {
	ret := make([]string, n, n)

	for val := 1; val <= n; val++ {
		idx := val - 1
		str := ""

		for _, rule := range conditions {
			str = str + rule(str, val)
		}

		ret[idx] = str
	}

	return ret
}
