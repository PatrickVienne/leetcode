package count_and_say

import (
	"fmt"
	"strings"
)

/*
The count-and-say sequence is a sequence of digit strings defined by the recursive formula:

countAndSay(1) = "1"
countAndSay(n) is the way you would "say" the digit string from countAndSay(n-1), which is then converted into a different digit string.
To determine how you "say" a digit string, split it into the minimal number of substrings such that each substring contains exactly one unique digit. Then for each substring, say the number of digits, then say the digit. Finally, concatenate every said digit.

For example, the saying and conversion for digit string "3322251":

Given a positive integer n, return the nth term of the count-and-say sequence.

Example 1:

Input: n = 1
Output: "1"
Explanation: This is the base case.
Example 2:

Input: n = 4
Output: "1211"
Explanation:
countAndSay(1) = "1"
countAndSay(2) = say "1" = one 1 = "11"
countAndSay(3) = say "11" = two 1's = "21"
countAndSay(4) = say "21" = one 2 + one 1 = "12" + "11" = "1211"

Constraints:

1 <= n <= 30
*/
func countAndSay(n int) string {
	word := []int{1}
	result := "1"
	for i := 1; i < n; i++ {
		newword := []int{}
		lastNumber := word[0]
		counts := 0
		for idx := range word {
			if word[idx] == lastNumber {
				counts++
			} else {
				newword = append(newword, counts)
				newword = append(newword, lastNumber)

				counts = 1
			}
			lastNumber = word[idx]
		}
		newword = append(newword, counts)
		newword = append(newword, lastNumber)
		word = newword
	}

	result = ""
	for idx := range word {
		result += fmt.Sprintf("%d", word[idx])
	}
	return result
}

func countAndSayRecursive(n int) string {
	if n == 1 {
		return "1"
	}

	prev := countAndSay(n - 1)
	var res strings.Builder
	i := 0
	c := 1

	for i < len(prev) {
		if i == len(prev)-1 {
			res.WriteByte(byte('0' + c))
			res.WriteByte(prev[i])
			break
		}

		if prev[i] == prev[i+1] {
			c++
		} else {
			res.WriteByte(byte('0' + c))
			res.WriteByte(prev[i])
			c = 1
		}

		i++
	}

	return res.String()
}
