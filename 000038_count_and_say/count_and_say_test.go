package count_and_say

import (
	"fmt"
	"testing"
)

var (
	in1a = 1
	in2a = 4
	out1 = "1"
	out2 = "1211"
)

func TestCase1(t *testing.T) {
	res := countAndSay(in1a)
	if res != out1 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := countAndSay(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func BenchmarkCountAndSay(b *testing.B) {
	/*

		goos: windows
		goarch: amd64
		pkg: leetcode/000038_count_and_say
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkCountAndSay-8   	 9513532	       107.6 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	leetcode/000038_count_and_say	1.815s

	*/
	for n := 0; n < b.N; n++ {
		countAndSay(in1a)
	}
}
