package numberof1bits

import (
	"fmt"
	"testing"
)

var (
	in1a  uint32 = 00000000000000000000000000001011
	out1a        = 3

	in2a  uint32 = 00000000000000000000000010000000
	out2a        = 1
)

func TestHammingWeight1(t *testing.T) {
	res := hammingWeight(uint32(in1a))
	if out1a != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1a, res))
	}
}

func TestHammingWeight2(t *testing.T) {
	res := hammingWeight(uint32(in2a))
	if out2a != res {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2a, res))
	}
}

func BenchmarkHammingWeight(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: leetcode/000191_number_of_1_bits
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkHammingWeight-8   	80807535	        19.42 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	leetcode/000191_number_of_1_bits	2.496s
	*/
	for n := 0; n < b.N; n++ {
		hammingWeight(in2a)
	}
}
