package unique_paths

import (
	"fmt"
	"testing"
)

var (
	in1a = 3
	in1b = 7
	out1 = 28

	in2a = 3
	in2b = 2
	out2 = 3

	in3a = 16
	in3b = 16
	out3 = 155117520
)

func TestCase1(t *testing.T) {
	res := uniquePaths(in1a, in1b)
	if res != out1 {
		t.Fatalf(fmt.Sprintf("Input: %v %v, Expected: %v, but was: %v", in1a, in1b, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := uniquePaths(in2a, in2b)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v %v, Expected: %v, but was: %v", in2a, in2b, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := uniquePaths(in3a, in3b)
	if res != out3 {
		t.Fatalf(fmt.Sprintf("Input: %v %v, Expected: %v, but was: %v", in3a, in3b, out3, res))
	}
}

func BenchmarkUniquePaths(b *testing.B) {
	for n := 0; n < b.N; n++ {
		uniquePaths(in2a, in2b)
	}
}
