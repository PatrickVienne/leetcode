package construct_binary_tree

/*
Given two integer arrays preorder and inorder where preorder is the preorder traversal of a binary tree and inorder is the inorder traversal of the same tree, construct and return the binary tree.



Example 1:


Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
Output: [3,9,20,null,null,15,7]
Example 2:

Input: preorder = [-1], inorder = [-1]
Output: [-1]


Constraints:

1 <= preorder.length <= 3000
inorder.length == preorder.length
-3000 <= preorder[i], inorder[i] <= 3000
preorder and inorder consist of unique values.
Each value of inorder also appears in preorder.
preorder is guaranteed to be the preorder traversal of the tree.
inorder is guaranteed to be the inorder traversal of the tree.
*/

// Definition for a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func buildTree(preorder []int, inorder []int) *TreeNode {
	if len(preorder) == 0 {
		return nil
	}
	if len(preorder) == 1 {
		return &TreeNode{Val: preorder[0]}
	}
	if len(preorder) == 2 {
		if preorder[0] == inorder[0] {
			return &TreeNode{Val: preorder[0], Right: &TreeNode{Val: preorder[1]}}
		} else {
			return &TreeNode{Val: preorder[0], Left: &TreeNode{Val: preorder[1]}}
		}
	}

	splitidx := 0
	for ; splitidx < len(inorder); splitidx++ {
		if inorder[splitidx] == preorder[0] {
			break
		}
	}
	return &TreeNode{Val: preorder[0], Left: buildTree(preorder[1:splitidx+1], inorder[:splitidx]), Right: buildTree(preorder[splitidx+1:], inorder[splitidx+1:])}
}
