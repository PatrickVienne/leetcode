package construct_binary_tree

import (
	"reflect"
	"testing"
)

var (
	preorder1 = []int{3, 9, 20, 15, 7}
	inorder1  = []int{9, 3, 15, 20, 7}
	out1      = &TreeNode{
		Val: 3,
		Left: &TreeNode{
			Val: 9,
		},
		Right: &TreeNode{
			Val: 20,
			Left: &TreeNode{
				Val: 15,
			},
			Right: &TreeNode{
				Val: 7,
			},
		},
	}

	preorder2 = []int{-1}
	inorder2  = []int{-1}
	out2      = &TreeNode{Val: -1}
)

func TestBuildTree1(t *testing.T) {
	res := buildTree(preorder1, inorder1)
	if !reflect.DeepEqual(res, out1) {
		t.Fatalf("wrong result:\nshould be %+v\nwas: %+v\n", out1, res)
	}
}

func TestBuildTree2(t *testing.T) {
	res := buildTree(preorder2, inorder2)
	if !reflect.DeepEqual(res, out2) {
		t.Fatalf("wrong result:\nshould be %+v\nwas: %+v\n", out2, res)
	}
}

func BenchmarkBuildTree(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: leetcode/000105_construct_binary_tree
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkBuildTree-8   	 2608411	       520.7 ns/op	     120 B/op	       5 allocs/op
		PASS
		ok  	leetcode/000105_construct_binary_tree	3.695s
	*/
	for n := 0; n < b.N; n++ {
		buildTree(preorder1, inorder1)
	}
}
