package threesum

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	in1a = []int{-1, 0, 1, 2, -1, -4}
	in2a = []int{0, 1, 1}
	in3a = []int{0, 0, 0}
	in4a = []int{1, 2, -2, -1}
	in5a = []int{3, 0, -2, -1, 1, 2}
	in6a = []int{-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6}
	in7a = []int{-1, 0, 1, 2, -1, -4, -2, -3, 3, 0, 4}
	out1 = [][]int{{-1, -1, 2}, {-1, 0, 1}}
	out2 = [][]int{}
	out3 = [][]int{{0, 0, 0}}
	out4 = [][]int{}
	out5 = [][]int{{-1, 0, 1}, {-2, 0, 2}, {-2, -1, 3}}
	out6 = [][]int{{-4, -2, 6}, {-4, 0, 4}, {-4, 1, 3}, {-4, 2, 2}, {-2, -2, 4}, {-2, 0, 2}}
	out7 = [][]int{{-4, 0, 4}, {-4, 1, 3}, {-3, -1, 4}, {-3, 0, 3}, {-3, 1, 2}, {-2, -1, 3}, {-2, 0, 2}, {-1, -1, 2}, {-1, 0, 1}}
)

func TestCase1(t *testing.T) {
	res := threeSum(in1a)
	if !assert.ElementsMatch(t, out1, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := threeSum(in2a)
	if !assert.ElementsMatch(t, out2, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := threeSum(in3a)
	if !assert.ElementsMatch(t, out3, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func TestCase4(t *testing.T) {
	res := threeSum(in4a)
	if !assert.ElementsMatch(t, out4, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in4a, out4, res))
	}
}

func TestCase5(t *testing.T) {
	res := threeSum(in5a)
	if !assert.ElementsMatch(t, out5, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in5a, out5, res))
	}
}

func TestCase6(t *testing.T) {
	res := threeSum(in6a)
	if !assert.ElementsMatch(t, out6, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in6a, out6, res))
	}
}

func TestCase7(t *testing.T) {
	res := threeSum(in7a)
	if !assert.ElementsMatch(t, out7, res) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in7a, out7, res))
	}
}

func BenchmarkThreeSum(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: leetcode/000015_three-sum
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkThreeSum-8   	 2148132	       536.8 ns/op	     144 B/op	       5 allocs/op
		PASS
		ok  	leetcode/000015_three-sum	2.561s
	*/
	for n := 0; n < b.N; n++ {
		threeSum(in1a)
	}
}
