package threesum

import "sort"

/*
Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.



Example 1:

Input: nums = [-1,0,1,2,-1,-4]
Output: [[-1,-1,2],[-1,0,1]]
Explanation:
nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0.
nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0.
nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0.
The distinct triplets are [-1,0,1] and [-1,-1,2].
Notice that the order of the output and the order of the triplets does not matter.
Example 2:

Input: nums = [0,1,1]
Output: []
Explanation: The only possible triplet does not sum up to 0.
Example 3:

Input: nums = [0,0,0]
Output: [[0,0,0]]
Explanation: The only possible triplet sums up to 0.


Constraints:

3 <= nums.length <= 3000
-105 <= nums[i] <= 105
*/
func threeSum(nums []int) [][]int {
	o := [][]int{}
	// sort array
	sort.Ints(nums)
	for i := range nums {
		// skip duplicates
		if i > 0 && nums[i-1] == nums[i] {
			continue
		}
		// two pointer, from next (j) and end (k)
		j, k := i+1, len(nums)-1
		for j < k {
			sum := nums[i] + nums[j] + nums[k]
			// if sum > 0, decrease k
			if sum > 0 {
				k--
				// if sum < 0, increase j
			} else if sum < 0 {
				j++
				// if sum == 0
			} else {
				o = append(o, []int{nums[i], nums[j], nums[k]})
				j++
				// move j until new value found
				for nums[j-1] == nums[j] && j < k {
					j++
				}

			}
		}
	}
	return o
}

func threeSum2(nums []int) [][]int {
	positives := make(map[int]int, 0)
	negatives := make(map[int]int, 0)
	zeros := 0

	counter := make(map[int]int, 0)
	results := make([][]int, 0)

	for idx := range nums {
		counter[nums[idx]]++
		if counter[nums[idx]] > 3 {
			continue
		}

		if nums[idx] > 0 {
			positives[nums[idx]]++
		} else if nums[idx] < 0 {
			negatives[nums[idx]]++
		} else {
			zeros++
		}
	}

	// zero case
	if zeros >= 3 {
		results = append(results, []int{0, 0, 0})
	}

	// 1 positive, 1 negative case
	if zeros >= 1 {
		for pos := range positives {
			if negatives[-pos] != 0 {
				results = append(results, []int{-pos, 0, pos})
			}
		}
	}

	// used numbers
	usednumbers := map[[3]int]bool{}

	// 1 positives, 2 negative case
	for neg := range negatives {

		negatives[neg]--
		for neg2, val := range negatives {
			if val == 0 {
				continue
			}
			if positives[-(neg2+neg)] != 0 {

				res := []int{neg, neg2, -(neg2 + neg)}
				sort.Ints(res)
				result := [3]int{res[0], res[1], res[2]}
				if usednumbers[result] {
					continue
				}
				results = append(results, res)
				usednumbers[result] = true
			}
		}
		negatives[neg]++
	}

	// 2 positives, 1 negative case
	for pos := range positives {
		positives[pos]--
		for pos2, val := range positives {
			if val == 0 {
				continue
			}
			if negatives[-(pos2+pos)] != 0 {
				res := []int{-(pos2 + pos), pos, pos2}
				sort.Ints(res)
				result := [3]int{res[0], res[1], res[2]}
				if usednumbers[result] {
					continue
				}
				results = append(results, res)
				usednumbers[result] = true
			}
		}
		positives[pos]++
	}
	return results
}
