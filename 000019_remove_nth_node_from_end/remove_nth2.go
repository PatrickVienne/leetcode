package remove_nth

/*
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.



Example 1:
Input: head = [1,2,3,4,5], n = 2
Output: [1,2,3,5]

Example 2:
Input: head = [1], n = 1
Output: []

Example 3:
Input: head = [1,2], n = 1
Output: [1]

Constraints:

The number of nodes in the list is sz.
1 <= sz <= 30
0 <= Node.val <= 100
1 <= n <= sz



Follow up: Could you do this in one pass?

*/

//Definition for singly-linked list.

func removeNthFromEnd2(head *ListNode, n int) *ListNode {
	ptr1 := head
	ptr2 := head

	for i := 0; i < n+1; i++ {
		for ptr2 == nil {
			head = head.Next
			return head
		}
		ptr2 = ptr2.Next
	}

	for ptr2 != nil {
		ptr1 = ptr1.Next
		ptr2 = ptr2.Next
	}

	ptr1.Next = ptr1.Next.Next
	return head

}

func removeNthFromEndFastest(head *ListNode, n int) *ListNode {
	dummy := head

	// count initial nodes in list
	var count int
	var headCount = head

	for headCount != nil {
		headCount = headCount.Next
		count++
	}

	// remove the nth element from the node
	if n == count {
		return head.Next
	}
	for n < count {
		if n == count-1 {
			head.Next = head.Next.Next
			n++
			continue
		}
		head = head.Next
		n++
	}

	return dummy

}
