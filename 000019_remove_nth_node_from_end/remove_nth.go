package remove_nth

/*
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.



Example 1:
Input: head = [1,2,3,4,5], n = 2
Output: [1,2,3,5]

Example 2:
Input: head = [1], n = 1
Output: []

Example 3:
Input: head = [1,2], n = 1
Output: [1]

Constraints:

The number of nodes in the list is sz.
1 <= sz <= 30
0 <= Node.val <= 100
1 <= n <= sz



Follow up: Could you do this in one pass?

*/

//Definition for singly-linked list.
type ListNode struct {
	Val  int
	Next *ListNode
}

func removeNthFromEnd(head *ListNode, n int) *ListNode {
	memory := make([]*ListNode, 0)
	walk := head

	i := 0
	for ; walk != nil; i++ {
		memory = append(memory, walk)
		walk = walk.Next
	}
	if i == n-1 {
		return head.Next
	}

	memoryIdx := i - n
	if memoryIdx == 0 {
		return head.Next
	}
	memory[memoryIdx-1].Next = memory[memoryIdx].Next
	return head

}
