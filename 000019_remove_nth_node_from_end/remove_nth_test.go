package remove_nth

import (
	"fmt"
	"reflect"
	"testing"
)

var (
	in1a = &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val: 4,
					Next: &ListNode{
						Val:  5,
						Next: nil,
					},
				},
			},
		},
	}
	in1b = 3

	out1 = &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 4,
				Next: &ListNode{
					Val:  5,
					Next: nil,
				},
			},
		},
	}

	in2a = &ListNode{
		Val: 1,
		Next: &ListNode{
			Val:  2,
			Next: nil,
		},
	}
	in2b = 1
	out2 = &ListNode{
		Val:  1,
		Next: nil,
	}

	in3a = &ListNode{
		Val:  1,
		Next: nil,
	}
	in3b = 1
)

func TestCase1(t *testing.T) {
	res := removeNthFromEnd(in1a, in1b)
	if !reflect.DeepEqual(out1, res) {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}

func TestCase2(t *testing.T) {
	res := removeNthFromEnd(in2a, in2b)
	if !reflect.DeepEqual(out2, res) {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}

func TestCase3(t *testing.T) {
	res := removeNthFromEnd(in3a, in3b)
	if res != nil {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}
