package remove_nth

import (
	"fmt"
	"reflect"
	"testing"
)

func TestCase21(t *testing.T) {
	res := removeNthFromEnd2(in1a, in1b)
	if !reflect.DeepEqual(out1, res) {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}

func TestCase22(t *testing.T) {
	res := removeNthFromEnd2(in2a, in2b)
	if !reflect.DeepEqual(out2, res) {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}

func TestCase23(t *testing.T) {
	res := removeNthFromEnd2(in3a, in3b)
	if res != nil {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}
