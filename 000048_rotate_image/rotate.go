package sudoku

/*
You are given an n x n 2D matrix representing an image, rotate the image by 90 degrees (clockwise).

You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.

 Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
Output: [[7,4,1],[8,5,2],[9,6,3]]

Constraints:

n == matrix.length == matrix[i].length
1 <= n <= 20
-1000 <= matrix[i][j] <= 1000

*/

func rotate(matrix [][]int) {
	// for 3x3 matrix allow 1 additional row
	var memory int
	var tmp int
	maxIdx := len(matrix) - 1

	// size 3x3, max row is index 1
	// size 4x4, max row is index 1
	maxRow := (maxIdx / 2)
	// size 3x3, max col is index 0
	// size 4x4, max col is index 1
	maxCol := (maxIdx - 1) / 2

	for row := 0; row <= maxRow; row++ {
		for col := 0; col <= maxCol; col++ {

			lastCol := col
			lastRow := row
			memory = matrix[lastRow][lastCol]
			for rot := 0; rot < 4; rot++ {
				newRow := lastCol
				newCol := maxIdx - lastRow
				tmp = matrix[newRow][newCol]
				matrix[newRow][newCol] = memory
				lastCol = newCol
				lastRow = newRow
				memory = tmp
			}

		}
	}
}
