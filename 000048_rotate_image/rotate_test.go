package sudoku

import (
	"fmt"
	"reflect"
	"testing"
)

var (
	in1a = [][]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}

	out1a = [][]int{
		{7, 4, 1},
		{8, 5, 2},
		{9, 6, 3},
	}

	in2a = [][]int{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 10, 11, 12},
		{13, 14, 15, 16},
	}

	out2a = [][]int{
		{13, 9, 5, 1},
		{14, 10, 6, 2},
		{15, 11, 7, 3},
		{16, 12, 8, 4},
	}
)

func TestRotate1(t *testing.T) {
	rotate(in1a)
	if !reflect.DeepEqual(in1a, out1a) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1a, in1a))
	}
}

func TestRotate2(t *testing.T) {
	rotate(in2a)
	if !reflect.DeepEqual(in2a, out2a) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2a, in2a))
	}
}

func BenchmarkRotate(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000048_rotate_image
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkRotate-8   	34160491	        40.34 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	000048_rotate_image	1.768s
	*/
	for n := 0; n < b.N; n++ {
		rotate(in2a)
	}
}
