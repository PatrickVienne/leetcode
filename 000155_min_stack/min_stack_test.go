package minstack

import (
	"testing"
)

var (
	in1a  uint32 = 00000000000000000000000000001011
	out1a        = 3

	in2a  uint32 = 00000000000000000000000010000000
	out2a        = 1

	// in3a  uint32 = 11111111111111111111111111111101
	// out3a        = 31
)

func TestMinStack(t *testing.T) {
	var res int

	minStack := Constructor()
	minStack.Push(-2)
	minStack.Push(0)
	minStack.Push(-3)
	res = minStack.GetMin() // return -3
	if res != -3 {
		t.Fatalf("first GetMin should return 3")
	}
	minStack.Pop()
	res = minStack.Top() // return 0
	if res != 0 {
		t.Fatalf("first GetMin should return 0")
	}
	res = minStack.GetMin() // return -2
	if res != -2 {
		t.Fatalf("first GetMin should return -2")
	}
}
