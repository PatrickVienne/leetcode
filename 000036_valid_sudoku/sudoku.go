package sudoku

import "strconv"

/*
Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

Each row must contain the digits 1-9 without repetition.
Each column must contain the digits 1-9 without repetition.
Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.
Note:

A Sudoku board (partially filled) could be valid but is not necessarily solvable.
Only the filled cells need to be validated according to the mentioned rules.

Example 1:
Input: board =
[["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
Output: true

Example 2:

Input: board =
[["8","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
Output: false
Explanation: Same as Example 1, except with the 5 in the top left corner being modified to 8. Since there are two 8's in the top left 3x3 sub-box, it is invalid.

Constraints:

board.length == 9
board[i].length == 9
board[i][j] is a digit 1-9 or '.'.

*/

func isValidSudoku(board [][]byte) bool {
	rowGroups := map[int]map[byte]bool{}
	colGroups := map[int]map[byte]bool{}
	for i := 0; i < 9; i++ {
		rowGroups[i] = make(map[byte]bool)
		colGroups[i] = make(map[byte]bool)
	}

	var boxnum int

	boxGroups := map[int]map[byte]bool{}
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			boxGroups[i+3*j] = make(map[byte]bool, 0)
		}
	}

	for rowidx, row := range board {
		for colidx, val := range row {
			if val == '.' {
				continue
			}

			boxnum = rowidx/3 + (colidx/3)*3

			if rowGroups[rowidx][val] || colGroups[colidx][val] || boxGroups[boxnum][val] {
				return false
			}

			rowGroups[rowidx][val] = true
			colGroups[colidx][val] = true
			boxGroups[boxnum][val] = true
		}
	}
	return true
}

func isValidSudokuFaster(board [][]byte) bool {
	rowsMap := [9][9]bool{}
	colsMap := [9][9]bool{}
	gridMap := [9][9]bool{}

	for row := 0; row < 9; row++ {
		for col := 0; col < 9; col++ {
			val, err := strconv.Atoi(string(board[row][col]))
			if err != nil {
				continue
			}
			val--
			gridIndex := col/3 + (row/3)*3
			if rowsMap[row][val] || colsMap[col][val] || gridMap[gridIndex][val] {
				return false
			}
			rowsMap[row][val] = true
			colsMap[col][val] = true
			gridMap[gridIndex][val] = true
		}
	}
	return true
}

func isValidSudokuFastest(board [][]byte) bool {
	R, C, S := [9]uint16{}, [9]uint16{}, [9]uint16{}
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if k, N := (i/3)*3+(j/3), uint16(1)<<(board[i][j]-'0'); board[i][j] != '.' {
				if R[i]&N == N {
					return false
				} else if C[j]&N == N {
					return false
				} else if S[k]&N == N {
					return false
				}
				R[i], C[j], S[k] = R[i]|N, C[j]|N, S[k]|N
			}
		}
	}
	return true
}
