package sudoku

import (
	"fmt"
	"testing"
)

var (
	in1a = [][]byte{
		{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
		{'6', '.', '.', '1', '9', '5', '.', '.', '.'},
		{'.', '9', '8', '.', '.', '.', '.', '6', '.'},
		{'8', '.', '.', '.', '6', '.', '.', '.', '3'},
		{'4', '.', '.', '8', '.', '3', '.', '.', '1'},
		{'7', '.', '.', '.', '2', '.', '.', '.', '6'},
		{'.', '6', '.', '.', '.', '.', '2', '8', '.'},
		{'.', '.', '.', '4', '1', '9', '.', '.', '5'},
		{'.', '.', '.', '.', '8', '.', '.', '7', '9'},
	}
	out1 = true

	in2a = [][]byte{
		{'8', '3', '.', '.', '7', '.', '.', '.', '.'},
		{'6', '.', '.', '1', '9', '5', '.', '.', '.'},
		{'.', '9', '8', '.', '.', '.', '.', '6', '.'},
		{'8', '.', '.', '.', '6', '.', '.', '.', '3'},
		{'4', '.', '.', '8', '.', '3', '.', '.', '1'},
		{'7', '.', '.', '.', '2', '.', '.', '.', '6'},
		{'.', '6', '.', '.', '.', '.', '2', '8', '.'},
		{'.', '.', '.', '4', '1', '9', '.', '.', '5'},
		{'.', '.', '.', '.', '8', '.', '.', '7', '9'},
	}
	out2 = false

	in3a = [][]byte{
		{'.', '.', '.', '.', '5', '.', '.', '1', '.'},
		{'.', '4', '.', '3', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '3', '.', '.', '1'},
		{'8', '.', '.', '.', '.', '.', '.', '2', '.'},
		{'.', '.', '2', '.', '7', '.', '.', '.', '.'},
		{'.', '1', '5', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '2', '.', '.', '.'},
		{'.', '2', '.', '9', '.', '.', '.', '.', '.'},
		{'.', '.', '4', '.', '.', '.', '.', '.', '.'},
	}
	out3 = false
)

func TestCase1(t *testing.T) {
	res := isValidSudoku(in1a)
	if res != out1 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := isValidSudoku(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := isValidSudoku(in3a)
	if res != out3 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func BenchmarkIsValidSudoku(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000036_valid_sudoku
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkIsValidSudoku-8   	  106597	     11134 ns/op	    3025 B/op	      57 allocs/op
		PASS
		ok  	000036_valid_sudoku	1.613s
	*/
	for n := 0; n < b.N; n++ {
		isValidSudoku(in1a)
	}
}

func BenchmarkIsValidSudokuFaster(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000036_valid_sudoku
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkIsValidSudokuFaster-8   	  317346	      3681 ns/op	    2448 B/op	      51 allocs/op
		PASS
		ok  	000036_valid_sudoku	1.496s
	*/
	for n := 0; n < b.N; n++ {
		isValidSudokuFaster(in1a)
	}
}

func BenchmarkIsValidSudokuFastest(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000036_valid_sudoku
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkIsValidSudokuFastest-8   	 4569189	       252.0 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	000036_valid_sudoku	1.709s
	*/
	for n := 0; n < b.N; n++ {
		isValidSudokuFastest(in1a)
	}
}
