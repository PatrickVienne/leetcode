package power_of_three

import (
	"fmt"
	"testing"
)

var (
	in1a = 10
	out1 = false

	in2a = 27
	out2 = true

	in3a = 1
	out3 = true

	bench = 1012
)

func TestCase1(t *testing.T) {
	res := isPowerOfThree(in1a)
	if res != out1 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func TestCase2(t *testing.T) {
	res := isPowerOfThree(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := isPowerOfThree(in3a)
	if res != out3 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func BenchmarkPowerOfThrees(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000326_power_of_three
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkPowerOfThrees-8   	243587508	         5.842 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	000326_power_of_three	2.850s
	*/
	for n := 0; n < b.N; n++ {
		isPowerOfThree(bench)
	}
}
