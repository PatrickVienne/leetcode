package binary_zig_zag_traversal

/*
Given the root of a binary tree, return the zigzag level order traversal of its nodes' values. (i.e., from left to right, then right to left for the next level and alternate between).



Example 1:


Input: root = [3,9,20,null,null,15,7]
Output: [[3],[20,9],[15,7]]
Example 2:

Input: root = [1]
Output: [[1]]
Example 3:

Input: root = []
Output: []


Constraints:

The number of nodes in the tree is in the range [0, 2000].
-100 <= Node.val <= 100
*/

// Definition for a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func zigzagLevelOrder(root *TreeNode) [][]int {
	// METHOD 1
	var res [][]int

	if root == nil {
		return res
	}

	q := []*TreeNode{root}
	level := 0

	for len(q) > 0 {
		res = append(res, []int{})
		for levelSize := len(q); levelSize > 0; levelSize-- {
			if q[0].Left != nil {
				q = append(q, q[0].Left)
			}
			if q[0].Right != nil {
				q = append(q, q[0].Right)
			}
			res[level] = append(res[level], q[0].Val)
			q = q[1:]
		}
		if level%2 != 0 {
			for i, j := 0, len(res[level])-1; i < j; i, j = i+1, j-1 {
				res[level][i], res[level][j] = res[level][j], res[level][i]
			}
		}
		level++
	}

	return res
	//     METHOD 2
	//     if root==nil {
	//         return [][]int{}
	//     }
	//     queue := []TreeNode{*root}
	//     tmpQueue := []TreeNode{}
	//     result := [][]int{}
	//     reverse := true

	//     for len(queue)>0 {
	//         resultLine := []int{}

	//         // add results depending on whether current line is reversed
	//         if reverse {
	//             for i := 0; i<len(queue);i++ {
	//                 resultLine = append(resultLine, queue[i].Val)
	//             }
	//         } else {
	//             for i := len(queue)-1; i>=0;i-- {
	//                 resultLine = append(resultLine, queue[i].Val)
	//             }
	//         }

	//         // collect nodes for next level
	//         for i := 0; i<len(queue);i++ {
	//             if queue[i].Left != nil {
	//                 tmpQueue = append(tmpQueue, *queue[i].Left)
	//             }
	//             if queue[i].Right != nil {
	//                 tmpQueue = append(tmpQueue, *queue[i].Right)
	//             }
	//         }

	//         result = append(result, resultLine)

	//         // set next level
	//         queue = tmpQueue[:]
	//         tmpQueue = []TreeNode{}
	//         reverse = !reverse
	//     }
	//     return result
}
