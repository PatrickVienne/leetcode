package binary_zig_zag_traversal

import (
	"reflect"
	"testing"
)

var (
	in1 = &TreeNode{
		Val: 3,
		Left: &TreeNode{
			Val: 9,
		},
		Right: &TreeNode{
			Val: 20,
			Left: &TreeNode{
				Val: 15,
			},
			Right: &TreeNode{
				Val: 7,
			},
		},
	}

	out1 = [][]int{{3}, {20, 9}, {15, 7}}

	in2  = &TreeNode{Val: 1}
	out2 = [][]int{{1}}

	in3  *TreeNode
	out3 [][]int
)

func TestZigzagLevelOrder1(t *testing.T) {
	res := zigzagLevelOrder(in1)
	if !reflect.DeepEqual(res, out1) {
		t.Fatalf("wrong result:\nshould be %+v\nwas: %+v\n", out1, res)
	}
}

func TestZigzagLevelOrder2(t *testing.T) {
	res := zigzagLevelOrder(in2)
	if !reflect.DeepEqual(res, out2) {
		t.Fatalf("wrong result:\nshould be %+v\nwas: %+v\n", out2, res)
	}
}

func TestZigzagLevelOrder3(t *testing.T) {
	res := zigzagLevelOrder(in3)
	if !reflect.DeepEqual(res, out3) {
		t.Fatalf("wrong result:\nshould be %+v\nwas: %+v\n", out3, res)
	}
}

func BenchmarkZigZagTree(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: leetcode/000103_binary_zig_zag_traversal
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkZigZagTree-8   	 1018790	      1113 ns/op	     304 B/op	      11 allocs/op
		PASS
		ok  	leetcode/000103_binary_zig_zag_traversal	2.748s
	*/
	for n := 0; n < b.N; n++ {
		zigzagLevelOrder(in1)
	}
}
