package linked_list_intersection

import (
	"fmt"
	"reflect"
	"testing"
)

var (
	out1 = &ListNode{
		Val: 8,
		Next: &ListNode{
			Val: 4,
			Next: &ListNode{
				Val:  5,
				Next: nil,
			},
		},
	}

	in1a = &ListNode{
		Val: 4,
		Next: &ListNode{
			Val:  1,
			Next: out1,
		},
	}

	in1b = &ListNode{
		Val: 5,
		Next: &ListNode{
			Val: 6,
			Next: &ListNode{
				Val:  1,
				Next: out1,
			},
		},
	}
)

func TestCase1(t *testing.T) {
	res := getIntersectionNode(in1a, in1b)
	if !reflect.DeepEqual(out1, res) {
		for res != nil {
			fmt.Printf("%d ", res.Val)
			res = res.Next
		}
		t.Fatalf("wrong result")
	}
}

func BenchmarkIntersection1(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: leetcode/000160_linked_list_intersection
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkIntersection2-8   	 3327578	       352.2 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	leetcode/000160_linked_list_intersection	2.156s
	*/
	for n := 0; n < b.N; n++ {
		getIntersectionNode(in1a, in1b)
	}
}

func BenchmarkIntersection2(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: leetcode/000160_linked_list_intersection
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkIntersection2-8   	12786074	        92.98 ns/op	       0 B/op	       0 allocs/op
		PASS
		ok  	leetcode/000160_linked_list_intersection	1.586s
	*/
	for n := 0; n < b.N; n++ {
		getIntersectionNode2(in1a, in1b)
	}
}
