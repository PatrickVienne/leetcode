package increasing_triplet_sub

import (
	"fmt"
	"testing"
)

var (
	in1a  = []int{1, 2, 3, 4, 5}
	out1a = true

	in2a = []int{5, 4, 3, 2, 1}
	out2 = false

	in3a = []int{2, 1, 5, 0, 4, 6}
	out3 = true

	in4a = []int{20, 100, 10, 12, 5, 13}
	out4 = true

	in5a = []int{4, 5, 2147483647, 1, 2}
	out5 = true
)

func TestCase1(t *testing.T) {
	res := increasingTriplet(in1a)
	if res != out1a {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1a, res))
	}
}

func TestCase2(t *testing.T) {
	res := increasingTriplet(in2a)
	if res != out2 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in2a, out2, res))
	}
}

func TestCase3(t *testing.T) {
	res := increasingTriplet(in3a)
	if res != out3 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in3a, out3, res))
	}
}

func TestCase4(t *testing.T) {
	res := increasingTriplet(in4a)
	if res != out4 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in4a, out4, res))
	}
}

func TestCase5(t *testing.T) {
	res := increasingTriplet(in5a)
	if res != out5 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in5a, out5, res))
	}
}

func BenchmarkTriplet(b *testing.B) {
	for n := 0; n < b.N; n++ {
		increasingTriplet(in3a)
	}
}
